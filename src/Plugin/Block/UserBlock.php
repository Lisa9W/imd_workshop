<?php

namespace Drupal\imd_render\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Header.
 *
 * @Block(
 *   id = "imd_user_block",
 *   admin_label = @Translation("IMD - User Block"),
 *   category = @Translation("Custom")
 * )
 */
class UserBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  public $age;

  public $gender;

  public $postalCode;

  public $city;

  public $newsletter;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $this->loadUserData(1);
    
    $build = [
      '#markup' => '<h1 class="foobar">User Profile</h1> <p># This is placeholder content</p>',
    ];

    return $build;
  }

  public function loadUserData($uid) {
    /**
     * @var User user
     */
    $user = $this->entityTypeManager->getStorage('user')->load($uid);

    if (!$user) {
      return;
    }

    if ($user->hasField('field_age')) {
      $this->age = $user->get('field_age')->value;
    }

    if ($user->hasField('field_gender')) {
      $this->gender = $user->get('field_gender')->value;
    }

    if ($user->hasField('field_city')) {
      $this->city = $user->get('field_city')->value;
    }

    if ($user->hasField('field_postal_code')) {
      $this->postalCode = $user->get('field_postal_code')->value;
    }

    if ($user->hasField('field_newsletter')) {
      $this->newsletter = $user->get('field_newsletter')->value;
    }
  }

}
